package com.example.preexamen_u1java;

public class ReciboNomina {

    // ATRIBUTOS
    private int numeroRecibo;
    private String nombre;
    private int horasTrabajadasNormal;
    private int horasTrabajadasExtra;
    private int puesto;
    private Float impuestoPorc;


    // CONSTRUCTORES
    public ReciboNomina(){
        this.numeroRecibo = 0;
        this.nombre = "";
        this.horasTrabajadasNormal = 0;
        this.horasTrabajadasExtra = 0;
        this.puesto = 0;
        this.impuestoPorc = 0.16f;
    }
    public ReciboNomina(int numeroRecibo, String nombre, int horasTrabajadasNormal, int horasTrabajadasExtra, int puesto, Float impuestoPorc) {
        this.numeroRecibo = numeroRecibo;
        this.nombre = nombre;
        this.horasTrabajadasNormal = horasTrabajadasNormal;
        this.horasTrabajadasExtra = horasTrabajadasExtra;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }


    // Encapsulamiento (GETs && SETs)
    public int getNumeroRecibo() {
        return numeroRecibo;
    }
    public void setNumeroRecibo(int numeroRecibo) {
        this.numeroRecibo = numeroRecibo;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getHorasTrabajadasNormal() {
        return horasTrabajadasNormal;
    }
    public void setHorasTrabajadasNormal(int horasTrabajadasNormal) {
        this.horasTrabajadasNormal = horasTrabajadasNormal;
    }
    public int getHorasTrabajadasExtra() {
        return horasTrabajadasExtra;
    }
    public void setHorasTrabajadasExtra(int horasTrabajadasExtra) {
        this.horasTrabajadasExtra = horasTrabajadasExtra;
    }
    public int getPuesto() {
        return puesto;
    }
    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }
    public Float getImpuestoPorc() {
        return impuestoPorc;
    }
    public void setImpuestoPorc(Float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }


    // METODOS
    public float calcularSubtotal(){
        //Caso: Puesto seleccionado
        if(this.puesto != 0){

            float pagoBase = 200.0f;
            switch(puesto){
                case 1: pagoBase += pagoBase*0.2f; break;
                case 2: pagoBase += pagoBase*0.5f; break;
                case 3: pagoBase += pagoBase; break;
            }

            float subTotal = 0.0f;
            subTotal = (pagoBase*this.horasTrabajadasNormal) + (this.horasTrabajadasExtra*pagoBase*2);

            return subTotal;
        }
        // Caso: Puesto no seleccionado
        else return 0.0f;
    }

    public float calcularImpuesto(){
        return this.calcularSubtotal() * this.impuestoPorc;
    }

    public float calcularTotal(){
        return this.calcularSubtotal() - this.calcularImpuesto();
    }
}
