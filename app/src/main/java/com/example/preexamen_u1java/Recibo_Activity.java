package com.example.preexamen_u1java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class Recibo_Activity extends AppCompatActivity {

    // Declaración componentes
    private TextView lblUsuario;
    private EditText txtNumRecibo;
    private EditText txtNombre;
    private EditText txtHorasNormal;
    private EditText txtHorasExtra;
    private RadioGroup rdgPuesto;
    private RadioButton rdbPuesto;
    private EditText txtSubtotal;
    private EditText txtImpuesto;
    private EditText txtTotal;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    // Declaración de clases
    private ReciboNomina reciboNomina;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);

        // Instaciamiento de componentes
        this.instanciarComponentes();

        // Asignación de metodos clic
        this.btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { btnCalcular_clic(); }
        });
        this.btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { btnRegresar_clic(); }
        });
        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { btnLimpiar_clic(); }
        });

        // Obtener Usuario con el paquete de información enviado
        Bundle paqueteInf = getIntent().getExtras();
        String usuario = paqueteInf.getString("NombreTrabajador").toString();
        this.lblUsuario.setText(usuario);

        // Obtener nombre desde los Strings guardados
        String nombre =this.getResources().getString(R.string.Nombre);
        this.txtNombre.setText(nombre);

        // Generar numero de recibo
        this.numAleatorio();
    }


    // Relación de componentes con el layout
    private void instanciarComponentes(){
        this.lblUsuario = findViewById(R.id.lblUsuario);
        this.txtNumRecibo = findViewById(R.id.txtNumeroRecibo);
        this.txtNombre = findViewById(R.id.txtNombre);
        this.txtHorasNormal = findViewById(R.id.txtHorasTrabajadas);
        this.txtHorasExtra = findViewById(R.id.txtHorasExtras);
        this.rdgPuesto = findViewById(R.id.rgPuestos);
        this.rdbPuesto = findViewById(rdgPuesto.getCheckedRadioButtonId());
        this.txtSubtotal = findViewById(R.id.txtSubtotal);
        this.txtImpuesto = findViewById(R.id.txtImpuesto);
        this.txtTotal = findViewById(R.id.txtTotalPagar);
        this.btnCalcular = findViewById(R.id.btnCalcular);
        this.btnLimpiar = findViewById(R.id.btnLimpiar);
        this.btnRegresar = findViewById(R.id.btnRegresar);
    }


    // Metodo clic del Button: btnCalcular
    private void btnCalcular_clic(){
        String strHorasNormal = this.txtHorasNormal.getText().toString();
        String strHorasExtra = this.txtHorasExtra.getText().toString();
        this.reciboNomina = new ReciboNomina();

        if(!strHorasNormal.equals("") && !strHorasExtra.equals("")){

            // Obtener horas normales y extras
            this.reciboNomina.setHorasTrabajadasNormal(Integer.parseInt(strHorasNormal));
            this.reciboNomina.setHorasTrabajadasExtra(Integer.parseInt(strHorasExtra));

            // Obtener puesto
            this.rdbPuesto = findViewById(rdgPuesto.getCheckedRadioButtonId());
            switch (rdbPuesto.getText().toString()){
                case "Auxiliar": this.reciboNomina.setPuesto(1); break;
                case "Albañil": this.reciboNomina.setPuesto(2); break;
                case "Ing. Obra": this.reciboNomina.setPuesto(3); break;
            }

            // Realizar calculos
            this.txtSubtotal.setText(Float.toString(this.reciboNomina.calcularSubtotal()));
            this.txtImpuesto.setText(Float.toString(this.reciboNomina.calcularImpuesto()));
            this.txtTotal.setText(Float.toString(this.reciboNomina.calcularTotal()));
        }
        else Toast.makeText(this, "Asegurece de ingresar ambas horas.", Toast.LENGTH_SHORT).show();
    }


    // Metodo clic para el Button: btnLimpiar
    private void btnLimpiar_clic(){
        this.txtHorasNormal.setText("");
        this.txtHorasExtra.setText("");

        RadioButton rdbAuxiliar = findViewById(R.id.rbAuxiliar);
        rdbAuxiliar.setChecked(true);

        this.txtSubtotal.setText("");
        this.txtImpuesto.setText("");
        this.txtTotal.setText("");

        this.reciboNomina = new ReciboNomina();

        this.numAleatorio();
    }


    // Metodo clic para el Button: btnRegresar
    private void btnRegresar_clic(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);

        confirmar.setTitle("Pago nomina.");
        confirmar.setMessage("¿Decea regresar al inicio?");

        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        confirmar.setNegativeButton("Cancelar", null);

        confirmar.show();
    }


    // Metodo para generar un numero aleatorio en el EditText: txtNumeroRecibo
    // Minimo : 10000
    // Maximo : 89999
    private void numAleatorio(){
        Random aleatorio = new Random();
        int numRecibo = aleatorio.nextInt(89999)+10000;
        this.txtNumRecibo.setText(Integer.toString(numRecibo));
    }
}