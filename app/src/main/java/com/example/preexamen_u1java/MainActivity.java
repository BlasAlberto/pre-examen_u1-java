package com.example.preexamen_u1java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Declaración de componentes
    private EditText txtNombreUsuario;
    private Button btnIngresar;
    private Button btnSalir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Instnciar componentes
        this.instanciarComponentes();

        // Asignación de eventos clic
        this.btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { btnIngresar_clic(); }
        });
        this.btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { btnSalir_clic(); }
        });
    }


    // Relación de componentes con el layout
    private void instanciarComponentes(){
        this.txtNombreUsuario = findViewById(R.id.txtNombreTrabajador);
        this.btnIngresar = (Button) findViewById(R.id.btnIngresar);
        this.btnSalir = findViewById(R.id.btnSalir);
    }


    private void btnIngresar_clic(){
        if(!this.txtNombreUsuario.getText().toString().equals("")){

           // Creación del paquete de información
           Bundle bundle = new Bundle();
           bundle.putString("NombreTrabajador", this.txtNombreUsuario.getText().toString());

           // Creación de Intent para llamar a otra actividad
           Intent intento = new Intent(MainActivity.this, Recibo_Activity.class);
           intento.putExtras(bundle);

           // Iniciar la actividad
           startActivity(intento);

           //// Reiniciar MainActivity
           this.txtNombreUsuario.setText("");

        }
        else Toast.makeText(this, "El nombre del trabajador es un requisito.", Toast.LENGTH_SHORT).show();
    }


    // Metodo clic para el Button: btnIngresar
    private void btnSalir_clic(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);

        confirmar.setTitle("Pago nomina.");
        confirmar.setMessage("¿Decea cerrar la aplicación?");

        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        confirmar.setNegativeButton("Cancelar", null);

        confirmar.show();
    }
}